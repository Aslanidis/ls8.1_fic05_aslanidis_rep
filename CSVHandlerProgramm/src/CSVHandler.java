import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVHandler {

	private String file;
	private String delimiter;
	private String line = "";

	// Constructor 1:
	public CSVHandler() {
		this(";", "studentNameCSV.txt");
	}


	// Constructor 2:
	public CSVHandler(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	
	// Begin Methods:
	public List<Schueler> getAll() {
		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();

		try {
			int vorname = 0;
			int nachname = 0;
			int joker = 0;
			int blamiert = 0;
			int fragen = 0;

			BufferedReader bf = new BufferedReader(new FileReader("studentNameCSV.csv"));
			String zeile = "";
			while (((zeile = bf.readLine()) != null) && zeile != ";") {
				String[] attributes = zeile.split(";");
				if (Arrays.stream(attributes).anyMatch("Vorname"::equals)) {
					vorname = Arrays.asList(attributes).indexOf("Vorname");
					nachname = Arrays.asList(attributes).indexOf("Name");
					joker = Arrays.asList(attributes).indexOf("joker");
					blamiert = Arrays.asList(attributes).indexOf("blamiert");
					fragen = Arrays.asList(attributes).indexOf("fragen");
				} else {
					students.add(new Schueler(attributes[vorname] + " " + attributes[nachname], Integer.parseInt(attributes[joker]),
							Integer.parseInt(attributes[blamiert]), Integer.parseInt(attributes[fragen])));
				}
			}
			bf.close();

		} catch (IOException e) {

		}

		return students;
	}

	public void printAll(List<Schueler> students) {
		String fom = "%30s%10s%15s%10s";
		System.out.printf(fom, "Name", "Joker", "Blamiert", "Fragen");
		System.out.println("");
		System.out.println(
				"----------------------------------------------------------------------------------------------");
		for (Schueler s : students) {
			System.out.printf(fmt, s.getName(), s.getJoker(), s.getBlamiert(), s.getFragen());
			System.out.println("");
		}
	}
}