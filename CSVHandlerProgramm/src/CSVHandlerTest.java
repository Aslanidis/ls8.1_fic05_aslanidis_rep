/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CSVHandlerTest {

  public static void main(String[] args) {
    CSVHandler csv = new CSVHandler();
    
    List<Schueler> students = csv.getAll();
    
    csv.printAll(students);
    
    try {
    	BufferedReader reader = new BufferedReader(new FileReader("studentName.csv"));
    	String zeile = "";
  
    	
    	while((zeile = reader.readLine()) != null){
    		split(zeile);
    		//System.out.println(zeile);
    	}
    	reader.close();
    	
    } catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
  }

private static void split(String str) {
	String [] wordlist = str.split(";");
	
	for (int i = 0; i < 5; i++) {
		switch(i) {
		case 0:
			System.out.printf("%-9s", wordlist[i]);
			break;
		case 1:
			System.out.printf("%-10s", wordlist[i]);
			break;
		case 2:
			System.out.printf("%-7s", wordlist[i]);
			break;
		case 3:
			System.out.printf("%-12s", wordlist[i]);
			break;
		case 4:
			System.out.printf("%-10s\n", wordlist[i]);
			break;
		}
	}
	
	  //anstelle von switch:
	  /*System.out.printf("%-10s", wordlist[i]);
	  }
	  System.out.println();*/
	 	
}
}
